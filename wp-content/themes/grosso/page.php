<?php get_header(); ?>
			
			<div id="content" class="clearfix row">
			
				<div id="main" class="col-sm-12 clearfix" role="main">

					<?php the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
							
							<section class="row post_content">
									<div id="subhead">
										<div class="container">	
										<?php if( get_field('sub_title') == '' ) : ?>
											<h1 class="post-title title"><?php the_title(); ?></h1>
										<?php else : ?>
											<h1 class="subtitle title"><?php the_field('sub_title'); ?></h1>
										<?php endif; ?>	
											<?php the_field('sub_content'); ?>
										</div>
									</div> <!-- end #subhead -->
									
									<div class="container">
										<?php the_content(); ?>
									</div>
							</section> <!-- end article header -->
						
						</article> <!-- end article -->

				</div> <!-- end #main -->
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
			</div> <!-- end #content -->

<?php get_footer(); ?>