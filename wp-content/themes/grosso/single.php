<?php get_header(); ?>
			
			<div id="content" class="clearfix row">
			
				<div id="main" class="col-sm-12 clearfix" role="main">

					<div class="container">

						<?php the_post(); ?>
						
						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
							
							<section class="row post_content">
									<h1 class="post-title title"><?php the_title(); ?></h1>
									<?php the_content(); ?>
							</section> <!-- end article header -->
						
						</article> <!-- end article -->

					</div>
			
				</div> <!-- end #main -->
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
			</div> <!-- end #content -->

<?php get_footer(); ?>