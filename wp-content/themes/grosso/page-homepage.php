<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>
			
			<?php the_post(); ?>

			<div id="subhead">
				<div class="container">	
					<h1 class="subtitle title"><?php the_field('sub_title'); ?></h1>
					<?php the_field('sub_content'); ?>
				</div>
			</div> <!-- end #subhead -->


			<div id="services">
				<div class="container">

				<?php
					$args = array(
						'post_type' => 'service',
						'posts_per_page' => -1,
						'order' => 'ASC'
					);
					$service = new WP_Query( $args );

					while ( $service->have_posts() ) {
						$service->the_post(); 

					if( has_post_thumbnail() ) :
						$image_id = get_post_thumbnail_id();
						$url = wp_get_attachment_image_src($image_id,'large', true);
					endif;
				?>
					<div class="col-md-6">
						<a href="<?php the_permalink(); ?>">
							<div class="service" style="background-image: url(<?php echo $url[0]; ?>);">
								<div class="title"><?php the_title(); ?></div>
							</div> <!-- end .service -->
						</a>
					</div>
				<?php
					}
					wp_reset_query();
				?>

				</div> <!-- end .container -->
			</div> <!-- end #services -->

			<div class="container">

				<div id="content" class="clearfix row">
				
					<div id="main" class="col-sm-8 clearfix" role="main">
						
						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
							
							<section class="row post_content">

									<?php /*<h3 class="title"><?php the_title(); ?></h3>*/ ?>
							
									<?php the_content(); ?>

							</section> <!-- end article header -->
						
						</article> <!-- end article -->
				
					</div> <!-- end #main -->
	    
					<div id="sidebar" class="col-sm-4" role="complementary">
						<?php the_field('sidebar_content'); ?>
					</div> <!-- end #sidebar -->
	    
				</div> <!-- end #content -->

			</div>

<?php get_footer(); ?>