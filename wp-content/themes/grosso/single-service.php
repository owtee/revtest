<?php get_header(); ?>
			
			<div id="content" class="clearfix row">
			
				<div id="main" class="col-sm-12 clearfix" role="main">

					<div class="container">

						<?php the_post(); ?>
						
						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
							
							<section class="row post_content">
									<h1 class="post-title title"><?php the_title(); ?></h1>
									<?php the_content(); ?>
							</section> <!-- end article header -->
						
						</article> <!-- end article -->

						<div id="recent_works">
							<h4>Recent Works:</h4>
							<?php /*<div class="col-md-3 work">
								<?php echo types_render_field("recent-works-image", array("separator"=>'</div><div class="col-md-3 work">', 'size' => 'thumbnail')); ?>
							</div>*/

								$rws = get_post_meta($post->ID,'wpcf-recent-works-image',false);

								if ( $rws[0] != '' ) :
								foreach ($rws as $rw) {
							?>
							<div class="col-xs-6 col-sm-4 col-md-3 work">
								<a href="<?php echo $rw; ?>"  rel="galleryx" class="workx"><img src="<?php echo str_replace('.jpg', '-150x150.jpg', $rw); ?>"></a>
							</div>
							<?php		
								}
								else :
							?>
									No recent works available.
							<?php
								endif;
							?>
						</div>

					</div>
			
				</div> <!-- end #main -->
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
			</div> <!-- end #content -->

<?php get_footer(); ?>