			<footer role="contentinfo">

				<div id="footer">

					<div id="upper-footer">

						<div class="container">
							<div class="col-md-6 testimonials">
								<h3>See What  Our Clients Have to Say</h3>

								<div class="testimonial">
									<div class="inner-testi">
										<div class="qbefore"></div>
										We would highly recommend your business for any type of construction needs
										<div class="qafter"></div>
									</div>
									<div class="testi-arrow"></div>
									<span>-Vesci Family</span>
								</div> <!-- end .testimonial -->

								<div class="testimonial">
									<div class="inner-testi">
										<div class="qbefore"></div>
										We hope our strive for environmental awareness will spark the same drive in others.
										<div class="qafter"></div>
									</div>
									<div class="testi-arrow"></div>
									<span>-Vesci Family</span>
								</div> <!-- end .testimonial -->

							</div> <!-- end .testimonials -->

							<div class="col-md-6 sponsors">
								<div class="sponsor">
									<h4>Accreditations and Reviews</h4>
									<a href="#" class="logo">
										<img src="<?php bloginfo('template_url'); ?>/images/logo/logo-bbb.png">
									</a>
									<a href="#" class="logo">
										<img src="<?php bloginfo('template_url'); ?>/images/logo/logo-home_advisor.png">
									</a>
								</div>
							</div> <!-- end .sponsors -->

							<div class="col-md-6 sponsors">
								<div class="sponsor">
									<h4>Featured Vendors</h4>
									<a href="#" class="logo">
										<img src="<?php bloginfo('template_url'); ?>/images/logo/logo-certainteed.png">
									</a>
									<a href="#" class="logo">
										<img src="<?php bloginfo('template_url'); ?>/images/logo/logo-gaf.png">
									</a>
								</div>
							</div> <!-- end .sponsors -->

						</div>

					</div>
			
					<div id="lower-footer" class="clearfix">

						<div class="container">

							<div class="col-xs-12 col-sm-6 col-md-6 address">
								<?php dynamic_sidebar('footer1'); ?>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-6 connect">
								<?php dynamic_sidebar('footer2'); ?>
							</div>
						
						</div> <!-- end #lower-footer -->

					</div>

				</div> <!-- end #footer -->
				
			</footer> <!-- end footer -->
				
		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->
		
		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>