<?php
/*
Template Name: Services
*/
?>

<?php get_header(); ?>
			
			<div id="content" class="clearfix row">
			
				<div id="main" class="col-sm-12 clearfix" role="main">

					<?php the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						<section class="row post_content">
								<div id="subhead">
									<div class="container">	
									<?php if( get_field('sub_title') == '' ) : ?>
										<h1 class="post-title title"><?php the_title(); ?></h1>
									<?php else : ?>
										<h1 class="subtitle title"><?php the_field('sub_title'); ?></h1>
									<?php endif; ?>	
										<?php the_field('sub_content'); ?>
									</div>
								</div> <!-- end #subhead -->
								
								<?php //the_content(); ?>

								<div id="inner-content">
									<div class="container">
									<?php
										$args = array(
											'post_type' => 'service',
											'posts_per_page' => -1,
											'order' => 'ASC'
										);

										$service = new WP_Query( $args );

										while ( $service->have_posts() ) {
											$service->the_post(); 
									?>
										<div class="col-md-6 servicez">
											<div class="col-md-4 image">
												<?php the_post_thumbnail('thumbnail'); ?>
											</div>
											<div class="col-md-8">
												<h4 class="stitle"><?php the_title(); ?></h4>
												<div class="content"><?php echo substr(get_the_excerpt(),0,140) . '...'; ?></div>
												<a class="readmore" href="<?php the_permalink(); ?>">Learn More</a>
											</div>
										</div>
									<?php
										}
										wp_reset_query();
									?>

										<div class="clearfix"></div>
									</div>
								</div>

						</section> <!-- end article header -->
					
					</article> <!-- end article -->

				</div> <!-- end #main -->
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
			</div> <!-- end #content -->

<?php get_footer(); ?>