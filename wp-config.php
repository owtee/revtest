<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'grosso' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'EbgQ9~7-BCHRu=/a-pLJG`^-@tN4r9oA/W1{vS~2+qI~Mi)RI4INnl2;RO(s{6ch');
define('SECURE_AUTH_KEY',  '#+|VjNI(]T[h::kmDPQG v/Mu+cE;4>^_~Ma;EnPHth^3%5)`3Pt_<04-j9cjJ%J');
define('LOGGED_IN_KEY',    'MJ-fzbSi(AEucTR|RuD{KZ.f|+5]0^u<$$!`^]+PF)6+[)cUs~N@B9k9/Tbg!*Gw');
define('NONCE_KEY',        'y.=*h,{&a _QM:Ic$JwUBlmrO$/~gmx!0FwHt~$v<b 3|{P6LpW6uy:U+Tq+*Q9N');
define('AUTH_SALT',        '_oV+=89&R^umgTbcVzz<m%lhd3Gn?Y07~4qNU(tunT}2}|,,5Eq-_TbQ#:Si:^jH');
define('SECURE_AUTH_SALT', 'lI<@byx/Ob95`wat:&W_33rPq9,$NPUE6DI9D}gGxu?8=E#k<Aftgo4+it4*m3_u');
define('LOGGED_IN_SALT',   'ix_WrN,TZDoH(O2[5/o|+#ux3!l}<,(}_ef,fHym@KURS+_s2AL<NCs:u.y-j={:');
define('NONCE_SALT',       'LcGtcUa262M9KBo7>bB~1Qo*V*5 cBqDh+);s&+QX(sEsd8^k|E-5D|i{[!>sOJ*');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
